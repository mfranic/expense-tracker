package hr.laplacian.scala.commons.authentication

import play.api.mvc._
import play.api.mvc.Results._
import scala.concurrent.Future
import play.api.http.HeaderNames._
import hr.laplacian.commons.authentication.service.AuthenticationService
import hr.laplacian.commons.asserts.Asserts
import hr.laplacian.commons.authentication.service.entity.AuthenticatedUser

case class Secured[A]
(
  action : Action[A]
)
(
  implicit private val authenticationService : AuthenticationService
) extends Action[A]
{
  Asserts.argumentIsNotNull(action)
  Asserts.argumentIsNotNull(authenticationService)

  def apply(request: Request[A]): Future[SimpleResult] =
  {
    request.headers.get(AUTHORIZATION).map{token =>

      authenticationService.validateToken(token) match
      {
        case true  =>
        {
          action(request)
        }
        case false => Future.successful(Unauthorized("Invalid authentication token"))
      }

    }.getOrElse(Future.successful(Unauthorized("Missing authentication token")))

  }

  override def parser: BodyParser[A] = action.parser
}

object Secured
{
  implicit def authenticatedUserFromSecuredRequest(implicit request : Request[Any], authenticationService : AuthenticationService) : AuthenticatedUser =
  {
    Asserts.argumentIsNotNull(request)
    Asserts.argumentIsNotNull(authenticationService)

    val token = request.headers.get(AUTHORIZATION).getOrElse(throw new IllegalStateException("Authorization token not found in secured endpoint"))

    authenticationService.getAuthenticatedUserFromToken(token)
  }
}

