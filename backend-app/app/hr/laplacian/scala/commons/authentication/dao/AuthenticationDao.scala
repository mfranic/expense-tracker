package hr.laplacian.commons.authentication.dao

import hr.laplacian.commons.authentication.domain.FullCredentials

trait AuthenticationDao
{
  def tryReadByUsername(username : String) : Option[FullCredentials]

  def create(credentials : FullCredentials) : FullCredentials
  def update(credentials : FullCredentials) : FullCredentials
  def delete(username : String) : FullCredentials

  def bulkReadByUserIds(userIds : Long*) : Seq[FullCredentials]
}
