package hr.laplacian.commons.authentication.dao

import org.springframework.stereotype.Repository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.namedparam.{MapSqlParameterSource, NamedParameterJdbcTemplate}
import scala.collection.JavaConversions._
import hr.laplacian.commons.asserts.Asserts
import hr.laplacian.commons.authentication.domain.FullCredentials

@Repository
class AuthenticationDaoImpl @Autowired
(
  private val jdbcTemplate : NamedParameterJdbcTemplate
) extends AuthenticationDao
{
  Asserts.argumentIsNotNull(jdbcTemplate)

  private final val CREATE_QUERY =
    """
      |INSERT INTO public.credentials
      |(
      |    username,
      |    password
      |)
      |VALUES
      |(
      |    :username,
      |    :password
      |)
      |
    """.stripMargin

  private final val TRY_READ_QUERY = "SELECT * FROM public.credentials WHERE username = :username"

  private final val BULK_READ_QUERY = "SELECT * FROM public.credentials WHERE user_id IN(%s)"

  private final val UPDATE_QUERY =
    """
      |UPDATE public.credentials
      |SET
      |username  = :username,
      |password  = :password
      |WHERE 
      |user_id   = :userId
    """.stripMargin

  private final val DELETE_QUERY = "DELETE FROM public.credentials WHERE username = :username"

  def tryReadByUsername(username: String): Option[FullCredentials] =
  {
    Asserts.argumentIsNotNull(username)

    val parametersMap = new MapSqlParameterSource("username", username)

    val credentials = jdbcTemplate.query(TRY_READ_QUERY, parametersMap, FullCredentialsRowMapper)

    credentials.ensuring(_.size < 2, "More than one credentials with same username found").headOption
  }

  def create(credentials: FullCredentials): FullCredentials =
  {
    Asserts.argumentIsNotNull(credentials)

    val parametersMap = new MapSqlParameterSource(Map("username" -> credentials.username,
                                                      "password" -> credentials.password))

    jdbcTemplate.update(CREATE_QUERY, parametersMap)

    tryReadByUsername(credentials.username).getOrElse(throw new RuntimeException("Credentials creation failed"))
  }

  def update(credentials: FullCredentials): FullCredentials =
  {
    Asserts.argumentIsNotNull(credentials)

    val parametersMap = new MapSqlParameterSource(Map("userId"   -> credentials.userId,
                                                      "username" -> credentials.username,
                                                      "password" -> credentials.password))

    jdbcTemplate.update(UPDATE_QUERY, parametersMap)

    tryReadByUsername(credentials.username).getOrElse(throw new RuntimeException("Credentials update failed"))
  }

  def delete(username: String): FullCredentials =
  {
    Asserts.argumentIsNotNull(username)

    val credentials = tryReadByUsername(username)

    Asserts.argumentIsTrue(credentials.isDefined, "Trying to delete non existing credentials")

    val parametersMap = new MapSqlParameterSource("username", username)

    jdbcTemplate.update(DELETE_QUERY, parametersMap)

    credentials.get

  }

  def bulkReadByUserIds(userIds: Long*): Seq[FullCredentials] =
  {
    Asserts.argumentIsNotNull(userIds)
    Asserts.argumentIsTrue(!userIds.isEmpty, "Trying to bulk read credentials with empty ")

    val userIdsString = userIds.mkString(",")

    val parametersMap = new MapSqlParameterSource()

    val credentials = jdbcTemplate.query(BULK_READ_QUERY.format(userIdsString), parametersMap, FullCredentialsRowMapper)

    credentials
  }
}
