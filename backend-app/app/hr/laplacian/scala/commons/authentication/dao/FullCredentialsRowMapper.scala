package hr.laplacian.commons.authentication.dao

import org.springframework.jdbc.core.RowMapper
import java.sql.ResultSet
import hr.laplacian.commons.authentication.domain.FullCredentials


object FullCredentialsRowMapper extends RowMapper[FullCredentials]
{
  def mapRow(resultSet: ResultSet, rowNum: Int): FullCredentials =
  {
    FullCredentials(
      userId               = resultSet.getLong   ("user_id"),
      username             = resultSet.getString ("username"),
      password             = resultSet.getString ("password")
    )
  }
}
