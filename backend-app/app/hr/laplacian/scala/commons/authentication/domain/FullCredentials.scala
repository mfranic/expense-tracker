package hr.laplacian.commons.authentication.domain

import hr.laplacian.commons.asserts.Asserts


case class FullCredentials
(
  username : String,
  userId   : Long,
  password : String
)
{
  Asserts.argumentIsNotNullNorEmpty(username)
  Asserts.argumentIsNotNull(userId)
  Asserts.argumentIsNotNull(password)
}
