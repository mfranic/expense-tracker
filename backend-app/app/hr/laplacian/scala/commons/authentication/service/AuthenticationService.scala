package hr.laplacian.commons.authentication.service

import hr.laplacian.commons.authentication.service.entity.{AuthenticatedUser, AuthenticationToken, MinimalCredentials}


trait AuthenticationService
{
  def authenticate(credentials : MinimalCredentials) : Option[AuthenticationToken]

  def validateToken(token : String) : Boolean
  def getAuthenticatedUserFromToken(token : String) : AuthenticatedUser

  def refreshToken(token : String) : Option[AuthenticationToken]
  def changePassword(token : String, newPassword : String)
}
