package hr.laplacian.commons.authentication.service

import org.springframework.stereotype.Service
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional
import org.joda.time.DateTime
import hr.laplacian.commons.asserts.Asserts
import hr.laplacian.commons.authentication.dao.AuthenticationDao
import hr.laplacian.commons.authentication.service.configuration.AuthenticationServiceConfiguration
import hr.laplacian.commons.jwt.{JwtUtil, JwtSecret}
import hr.laplacian.commons.authentication.service.entity.{AuthenticatedUser, TokenPayload, AuthenticationToken, MinimalCredentials}
import hr.laplacian.commons.authentication.domain.FullCredentials


@Service
@Transactional
class AuthenticationServiceImpl @Autowired
(
  private val authenticationDao : AuthenticationDao,
  private val config : AuthenticationServiceConfiguration
) extends AuthenticationService
{
  Asserts.argumentIsNotNull(authenticationDao)

  implicit private final val SECRET = JwtSecret(config.secretString)

  def authenticate(credentials: MinimalCredentials): Option[AuthenticationToken] =
  {
    Asserts.argumentIsNotNull(credentials)

    authenticationDao.tryReadByUsername(credentials.username).filter(_.password == credentials.password
    ).map( createAuthenticationToken(_))
  }

  def validateToken(token: String): Boolean =
  {
    Asserts.argumentIsNotNullNorEmpty(token)

    JwtUtil.getPayloadIfValidToken[TokenPayload](token).filter(_.exp.isAfterNow).isDefined
  }

  def getAuthenticatedUserFromToken(token : String) : AuthenticatedUser =
  {
    Asserts.argumentIsNotNullNorEmpty(token)
    Asserts.argumentIsTrue(validateToken(token))

    AuthenticatedUser(JwtUtil.getPayloadIfValidToken[TokenPayload](token).get.userId)
  }

  def refreshToken(token : String) : Option[AuthenticationToken] =
  {
    Asserts.argumentIsNotNullNorEmpty(token)

    JwtUtil.getPayloadIfValidToken[TokenPayload](token).map{ payload =>
      authenticationDao.bulkReadByUserIds(payload.userId).ensuring(_.length == 1,"Not exactly one user exists for user id from token payload").head
    }.map(createAuthenticationToken(_))
  }

  def changePassword(token : String, newPassword : String)
  {
    Asserts.argumentIsNotNull(token)
    Asserts.argumentIsNotNull(newPassword)

    JwtUtil.getPayloadIfValidToken[TokenPayload](token).ensuring(_.isDefined).foreach{ payload =>
      val credentials = authenticationDao.tryReadByUsername(payload.username).getOrElse(throw new IllegalStateException("Trying to change password for non existing username"))

      authenticationDao.update(
        FullCredentials(username             = credentials.username,
                        userId               = credentials.userId,
                        password             = newPassword)
      )
    }
  }

  private def createAuthenticationToken(credentials : FullCredentials) : AuthenticationToken =
  {
    Asserts.argumentIsNotNull(credentials)

    val tokenPayload = TokenPayload(credentials.userId, credentials.username, DateTime.now().plusHours(config.tokenHoursToLive))

    AuthenticationToken(JwtUtil.signJwtPayload(tokenPayload))
  }

}
