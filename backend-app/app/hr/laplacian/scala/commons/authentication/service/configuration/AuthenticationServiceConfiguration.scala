package hr.laplacian.commons.authentication.service.configuration

import hr.laplacian.commons.asserts.Asserts

case class AuthenticationServiceConfiguration
(
  secretString     :String,
  tokenHoursToLive : Int
)
{
  Asserts.argumentIsNotNullNorEmpty(secretString)
  Asserts.argumentIsNotNull(tokenHoursToLive)
  Asserts.argumentIsTrue(tokenHoursToLive > 0)
}


