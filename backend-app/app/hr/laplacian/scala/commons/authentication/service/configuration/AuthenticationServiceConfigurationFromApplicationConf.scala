package hr.laplacian.commons.authentication.service.configuration

import org.springframework.stereotype.Component
import play.api.Play.current

@Component
class AuthenticationServiceConfigurationFromApplicationConf extends AuthenticationServiceConfiguration(
  current.configuration.getString("jwt.token.secret").getOrElse(throw new IllegalStateException("jwt.token.secret not specified in application.conf")),
  current.configuration.getInt("jwt.token.hoursToLive").getOrElse(throw new IllegalStateException("jwt.token.hoursToLive not specified in application.conf"))
)
