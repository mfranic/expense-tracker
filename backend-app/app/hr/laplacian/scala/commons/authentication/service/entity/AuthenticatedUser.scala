package hr.laplacian.commons.authentication.service.entity

import hr.laplacian.commons.asserts.Asserts


case class AuthenticatedUser
(
  userId : Long
)
{
  Asserts.argumentIsNotNull(userId)
}
