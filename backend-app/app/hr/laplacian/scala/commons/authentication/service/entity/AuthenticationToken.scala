package hr.laplacian.commons.authentication.service.entity

import play.api.libs.json.{Format, Json}
import hr.laplacian.commons.asserts.Asserts


case class AuthenticationToken
(
  token : String
)
{
  Asserts.argumentIsNotNull(token)
}

object AuthenticationToken
{
  implicit val jsonFormat : Format[AuthenticationToken] = Json.format[AuthenticationToken]
}
