package hr.laplacian.commons.authentication.service.entity

import play.api.libs.json.{Json, Format}
import hr.laplacian.commons.asserts.Asserts

case class ChangePasswordRequest
(
  newPassword : String
)
{
  Asserts.argumentIsNotNullNorEmpty(newPassword)
}

object ChangePasswordRequest
{
  implicit val jsonFormat : Format[ChangePasswordRequest] = Json.format[ChangePasswordRequest]
}
