package hr.laplacian.commons.authentication.service.entity

import play.api.libs.json.{Json, Format}
import hr.laplacian.commons.asserts.Asserts

case class MinimalCredentials
(
  username : String,
  password : String
)
{
  Asserts.argumentIsNotNullNorEmpty(username)
  Asserts.argumentIsNotNullNorEmpty(password)
}

object MinimalCredentials
{
  implicit val jsonFormat : Format[MinimalCredentials] = Json.format[MinimalCredentials]
}
