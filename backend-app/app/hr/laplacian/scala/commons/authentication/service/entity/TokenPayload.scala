package hr.laplacian.commons.authentication.service.entity

import play.api.libs.json.{Json, Format}
import org.joda.time.DateTime
import hr.laplacian.commons.asserts.Asserts


case class TokenPayload
(
  userId   : Long,
  username : String,
  exp      : DateTime
)
{
  Asserts.argumentIsNotNull(userId)
  Asserts.argumentIsNotNull(username)
  Asserts.argumentIsNotNull(exp)
}

object TokenPayload
{
  implicit val jsonFormat : Format[TokenPayload] = Json.format[TokenPayload]
}
