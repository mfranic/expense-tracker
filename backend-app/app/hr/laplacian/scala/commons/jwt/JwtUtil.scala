package hr.laplacian.commons.jwt

import play.api.libs.json.{Reads, Writes, JsValue, Json}
import com.nimbusds.jose.{Payload, JWSAlgorithm, JWSHeader, JWSObject}
import hr.laplacian.commons.asserts.Asserts


object JwtUtil
{
  def signJwtPayload(payload : String)(implicit secret : JwtSecret) : String =
  {
    Asserts.argumentIsNotNull(payload)

    val jwsObject = new JWSObject(new JWSHeader(JWSAlgorithm.HS256), new Payload(payload))
    jwsObject.sign(secret.signer)
    jwsObject.serialize
  }

  def signJwtPayload(payload : JsValue)(implicit secret : JwtSecret) : String = signJwtPayload(payload.toString)
  def signJwtPayload[T](payload : T)(implicit secret : JwtSecret, jsonWrites : Writes[T]) : String = signJwtPayload(Json.toJson(payload))

  def getPayloadStringIfValidToken(token : String)(implicit secret : JwtSecret) : Option[String] =
  {
    Asserts.argumentIsNotNullNorEmpty(token)

    try
    {
      val jwsObject = JWSObject.parse(token)

      jwsObject.verify(secret.verifier) match
      {
        case true  => Some(jwsObject.getPayload.toString)
        case false => None
      }
    }
    catch
    {
      case  _ : Throwable => None
    }
  }

  def getPayloadIfValidToken[T](token : String)(implicit secret : JwtSecret, jsonWrites : Reads[T]) : Option[T] =
  {
    Asserts.argumentIsNotNullNorEmpty(token)

    getPayloadStringIfValidToken(token).map(Json.parse(_).as[T])
  }
}
