package hr.laplacian.scala.commons.utils

import java.math.{BigDecimal => JavaBigDecimal}
import hr.laplacian.commons.asserts.Asserts

object BigDecimalUtils
{
  def optionScalaToOptionJava(value: Option[BigDecimal]): Option[JavaBigDecimal] =
  {
    Asserts.argumentIsNotNull(value)

   if(value.isDefined) Option(BigDecimalUtils.scalaToJava(value.get)) else Option.empty
  }

  def scalaToJava(value: BigDecimal) =
  {
    Asserts.argumentIsNotNull(value)

    new JavaBigDecimal(value.toString)
  }

  def optionJavaToOptionScala(value: Option[JavaBigDecimal]): Option[BigDecimal] =
  {
    Asserts.argumentIsNotNull(value)

    if(value.isDefined) Option(BigDecimalUtils.javaToScala(value.get)) else Option.empty
  }

  def javaToScala(value: JavaBigDecimal) =
  {
    Asserts.argumentIsNotNull(value)

    BigDecimal(value)
  }
}
