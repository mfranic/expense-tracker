package hr.laplacian.scala.commons.utils

import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}
import org.joda.time._
import java.util.Date
import hr.laplacian.commons.asserts.Asserts


object DateUtils
{
  final val dateTimeFormatter             : DateTimeFormatter = DateTimeFormat.forPattern("dd MMMM YYYY");
  final val dateTimeFormatterWithMinutes  : DateTimeFormatter = DateTimeFormat.forPattern("dd MMMM YYYY, HH:mm");
  final val dateTimeFormatterWithSeconds  : DateTimeFormatter = DateTimeFormat.forPattern("dd MMMM YYYY HH:mm:ss");
  final val DD_MMM_YYYY__dateFormatter    : DateTimeFormatter = DateTimeFormat.forPattern("dd MMM YYYY");
  final val POSTGRES_DATE_FORMAT          : DateTimeFormatter = DateTimeFormat.forPattern("YYYY-MM-dd")
  final val HH_mm_DATE_FORMATTER          : DateTimeFormatter = DateTimeFormat.forPattern("HH:mm")
  final val YYYY_MM_dd__dateFormatter     : DateTimeFormatter = DateTimeFormat.forPattern("YYYY-MM-dd");

  def nowDateTimeUTC(): DateTime =
  {
    DateTime.now(DateTimeZone.UTC)
  }

  def nowLocalDateUTC(): LocalDate =
  {
    LocalDate.now(DateTimeZone.UTC)
  }

  def jodaDateTimeToJavaDate(value: Option[DateTime]) : Option[Date] =
  {
    Asserts.argumentIsNotNull(value)

    value.map(v=>DateUtils.jodaDateTimeToJavaDate(v))
  }
  def jodaDateTimeToJavaDate(value: DateTime): Date =
  {
    Asserts.argumentIsNotNull(value)

    value.toDate
  }
  def javaDateToJodaDateTime(value: Date): DateTime =
  {
    Asserts.argumentIsNotNull(value)

    new DateTime(value).withZone(DateTimeZone.UTC)
  }
  def javaDateToJodaDateTime(value: Option[Date]): Option[DateTime] =
  {
    Asserts.argumentIsNotNull(value)

    value.map(v=>DateUtils.javaDateToJodaDateTime(v))
  }

  def jodaLocalDateToJavaDate(value: Option[LocalDate]) : Option[Date] =
  {
    Asserts.argumentIsNotNull(value)

    value.map(v=>DateUtils.jodaLocalDateToJavaDate(v))
  }
  def jodaLocalDateToJavaDate(value: LocalDate): Date =
  {
    Asserts.argumentIsNotNull(value)

    value.toDate
  }
  def javaDateToJodaLocalDate(value: Date): LocalDate =
  {
    Asserts.argumentIsNotNull(value)

    new LocalDate(value, DateTimeZone.UTC)
  }
  def javaDateToJodaLocalDate(value: Option[Date]): Option[LocalDate] =
  {
    Asserts.argumentIsNotNull(value)

    value.map(v=>DateUtils.javaDateToJodaLocalDate(v))
  }

  def jodaLocalTimeToJavaDate(value: Option[LocalTime]) : Option[Date] =
  {
    Asserts.argumentIsNotNull(value)

    value.map(DateUtils.jodaLocalTimeToJavaDate(_))
  }
  def jodaLocalTimeToJavaDate(value: LocalTime): Date =
  {
    Asserts.argumentIsNotNull(value)

    new Date(value.toDateTimeToday.getMillisOfDay)
  }
  def javaDateToJodaLocalTime(value: Date): LocalTime =
  {
    Asserts.argumentIsNotNull(value)

    new LocalTime(value)
  }
  def javaDateToJodaLocalTime(value: Option[Date]): Option[LocalTime] =
  {
    Asserts.argumentIsNotNull(value)

    value.map(DateUtils.javaDateToJodaLocalTime(_))
  }

  def calculateLocalTimeRange(from :LocalTime, to :LocalTime, step :Period) :Seq[LocalTime] =
  {
    Iterator.iterate(from)(_.plus(step)).takeWhile(!_.isAfter(to)).toList
  }
}

