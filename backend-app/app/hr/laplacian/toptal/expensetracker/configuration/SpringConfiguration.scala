package hr.laplacian.configuration

import play.api.db.DB
import org.springframework.context.annotation.{Bean, ComponentScan, Configuration}
import org.springframework.transaction.annotation.EnableTransactionManagement
import org.springframework.jdbc.datasource.DataSourceTransactionManager
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate

@Configuration
@ComponentScan(basePackages = Array("controllers", "hr.laplacian.scala.commons.authentication", "hr.laplacian.toptal.expensetracker"))
@EnableTransactionManagement
class SpringConfiguration
{
  @Bean
  def dataSource =
  {
    import play.api.Play.current

    DB.getDataSource()
  }

  @Bean
  def transactionManager =
  {
    val transactionManager = new DataSourceTransactionManager
    transactionManager.setDataSource(dataSource)
    transactionManager
  }

  @Bean
  def jdbcTemplate =
  {
    new NamedParameterJdbcTemplate(dataSource)
  }

}
