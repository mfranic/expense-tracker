package hr.laplacian.toptal.expensetracker.modules.expense.dao

import hr.laplacian.toptal.expensetracker.modules.expense.domain.Expense


trait ExpenseDao
{
  def create (expense   : Expense) : Expense
  def tryRead(expenseId : Long)    : Option[Expense]
  def update (expense   : Expense) : Expense
  def delete (expenseId : Long)    : Expense

}
