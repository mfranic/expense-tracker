package hr.laplacian.toptal.expensetracker.modules.expense.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import org.springframework.jdbc.core.namedparam.{MapSqlParameterSource, NamedParameterJdbcTemplate}
import hr.laplacian.commons.asserts.Asserts
import hr.laplacian.toptal.expensetracker.modules.expense.domain.Expense
import org.springframework.jdbc.support.GeneratedKeyHolder
import scala.collection.JavaConversions._
import hr.laplacian.scala.commons.utils.{BigDecimalUtils, DateUtils}

@Repository
class ExpenseDaoImpl @Autowired
(
  private val jdbcTemplate : NamedParameterJdbcTemplate
) extends ExpenseDao
{
  Asserts.argumentIsNotNull(jdbcTemplate)

  private final val CREATE_QUERY =
    """
      |INSERT INTO expenses
      |(
      |   datetime,
      |   description,
      |   amount,
      |   comment
      |)
      |VALUES
      |(
      |   :datetime,
      |   :description,
      |   :amount,
      |   :comment
      |)
    """.stripMargin

  private final val TRY_READ_QUERY = "SELECT * FROM expenses WHERE id = :id"

  private final val UPDATE_QUERY =
    """
      |UPDATE expenses
      |SET
      |   datetime    = :datetime,
      |   description = :description,
      |   amount      = :amount,
      |   comment     = :comment
      |WHERE id = :id
    """.stripMargin

  private final val DELETE_QUERY = "DELETE FROM expenses WHERE id = :id"

  def create(expense: Expense): Expense =
  {
    Asserts.argumentIsNotNull(expense)

    val parametersMap = new MapSqlParameterSource(Map("datetime"    -> DateUtils.jodaDateTimeToJavaDate(expense.datetime),
                                                      "description" -> expense.description,
                                                      "amount"      -> BigDecimalUtils.scalaToJava(expense.amount),
                                                      "comment"     -> expense.comment.getOrElse(null)))

    var generatedKey : GeneratedKeyHolder = new GeneratedKeyHolder

    jdbcTemplate.update(CREATE_QUERY, parametersMap, generatedKey)

    tryRead(generatedKey.getKeys.get("id").asInstanceOf[Number].longValue).ensuring(_.isDefined, "Creation of expense failed!").get
  }

  def tryRead(expenseId: Long): Option[Expense] =
  {
    Asserts.argumentIsNotNull(expenseId)

    val parametersMap = new MapSqlParameterSource("id", expenseId)

    val expenses = jdbcTemplate.query(TRY_READ_QUERY, parametersMap, ExpenseRowMapper)

    expenses.ensuring(_.size < 2, "More than one expense with same id found").headOption
  }

  def update(expense: Expense): Expense =
  {
    Asserts.argumentIsNotNull(expense)
    Asserts.argumentIsTrue(expense.id.isDefined)

    val parametersMap = new MapSqlParameterSource(Map("id"          -> expense.id.get,
                                                      "datetime"    -> DateUtils.jodaDateTimeToJavaDate(expense.datetime),
                                                      "description" -> expense.description,
                                                      "amount"      -> BigDecimalUtils.scalaToJava(expense.amount),
                                                      "comment"     -> expense.comment.getOrElse(null)))

    jdbcTemplate.update(UPDATE_QUERY, parametersMap)

    tryRead(expense.id.get).getOrElse(throw new RuntimeException("Expense update failed"))
  }

  def delete(expenseId: Long): Expense =
  {
    Asserts.argumentIsNotNull(expenseId)

    val expense = tryRead(expenseId)

    Asserts.argumentIsTrue(expense.isDefined, "Trying to delete non existing expense")

    val parametersMap = new MapSqlParameterSource("id", expenseId)

    jdbcTemplate.update(DELETE_QUERY, parametersMap)

    expense.get
  }
}
