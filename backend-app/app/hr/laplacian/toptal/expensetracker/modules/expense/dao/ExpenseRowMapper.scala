package hr.laplacian.toptal.expensetracker.modules.expense.dao

import org.springframework.jdbc.core.RowMapper
import hr.laplacian.toptal.expensetracker.modules.expense.domain.Expense
import java.sql.ResultSet
import hr.laplacian.scala.commons.utils.{BigDecimalUtils, DateUtils}


object ExpenseRowMapper  extends RowMapper[Expense]
{
  def mapRow(resultSet: ResultSet, rowNum: Int): Expense =
  {
    Expense (
      id           = Option(                          resultSet.getLong(      "id")),
       datetime    = DateUtils.javaDateToJodaDateTime(resultSet.getTimestamp( "datetime")),
       description =                                  resultSet.getString(    "description"),
       amount      = BigDecimalUtils.javaToScala(     resultSet.getBigDecimal("amount")),
       comment     = Option(                          resultSet.getString(    "comment"))
    )
  }
}
