package hr.laplacian.toptal.expensetracker.modules.expense.domain

import org.joda.time.DateTime
import hr.laplacian.commons.asserts.Asserts


case class Expense
(
  id          : Option[Long],
  datetime    : DateTime,
  description : String,
  amount      : BigDecimal,
  comment     : Option[String]
)
{
  Asserts.argumentIsNotNull(id)
  Asserts.argumentIsNotNull(datetime)
  Asserts.argumentIsNotNullNorEmpty(description)
}
