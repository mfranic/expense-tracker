package hr.laplacian.toptal.expensetracker.modules.expense.service

import hr.laplacian.toptal.expensetracker.modules.expense.domain.Expense


trait ExpenseService
{
  def create (expense   : Expense) : Expense
  def tryRead(expenseId : Long)    : Option[Expense]
  def update (expense   : Expense) : Expense
  def delete (expenseId : Long)    : Expense
}
