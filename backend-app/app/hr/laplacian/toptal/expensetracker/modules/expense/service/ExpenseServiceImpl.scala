package hr.laplacian.toptal.expensetracker.modules.expense.service

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.beans.factory.annotation.Autowired
import hr.laplacian.toptal.expensetracker.modules.expense.dao.ExpenseDao
import hr.laplacian.toptal.expensetracker.modules.expense.domain.Expense
import hr.laplacian.commons.asserts.Asserts

@Service
@Transactional
class ExpenseServiceImpl @Autowired
(
  private val expenseDao : ExpenseDao
) extends ExpenseService
{
  def create(expense: Expense): Expense =
  {
    Asserts.argumentIsNotNull(expense)

    expenseDao.create(expense)
  }

  def tryRead(expenseId: Long): Option[Expense] =
  {
    Asserts.argumentIsNotNull(expenseId)

    expenseDao.tryRead(expenseId)
  }

  def update(expense: Expense): Expense =
  {
    Asserts.argumentIsNotNull(expense)

    expenseDao.update(expense)
  }

  def delete(expenseId: Long): Expense =
  {
    Asserts.argumentIsNotNull(expenseId)

    expenseDao.delete(expenseId)
  }
}
