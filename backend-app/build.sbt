import com.typesafe.config.ConfigFactory

val conf = ConfigFactory.parseFile(new File("conf/application.conf")).resolve()

name := conf.getString("app.name")

version := conf.getString("app.version")

val springVersion = "3.2.2.RELEASE"

libraryDependencies ++= Seq(
  jdbc,
  "postgresql" % "postgresql" % "9.1-901.jdbc4",
  "org.springframework" % "spring-context" % springVersion,
  "org.springframework" % "spring-aop" % springVersion,
  "org.springframework" % "spring-tx" % springVersion,
  "org.springframework" % "spring-jdbc" % springVersion,
  "com.nimbusds" % "nimbus-jose-jwt" % "2.25",
  anorm,
  cache
)

play.Project.playScalaSettings
