BEGIN;

 CREATE TABLE public.db_version
 (
   version TEXT DEFAULT 1 NOT NULL
 );

 INSERT INTO public.db_version
 ( version )
   VALUES ( '1' );

CREATE TABLE public.expenses
(
  id SERIAL  PRIMARY KEY NOT NULL,
  datetime TIMESTAMP NOT NULL,
  description TEXT NOT NULL,
  amount NUMERIC  NOT NULL,
  comment TEXT NOT NULL
);

CREATE TABLE public.users
(
  id bigint PRIMARY KEY NOT NULL,
  first_name character varying(32) NOT NULL,
  middle_name character varying(32),
  last_name character varying(32) NOT NULL,
  currency character varying(3) NOT NULL
);

COMMIT;