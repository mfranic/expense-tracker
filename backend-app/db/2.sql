BEGIN;

INSERT INTO public.db_version
(
  version
)
VALUES ( '2' );

CREATE TABLE public.credentials
(
  user_id SERIAL PRIMARY KEY NOT NULL,
  username character varying(16) NOT NULL,
  password character varying(16) NOT NULL
);

COMMIT;