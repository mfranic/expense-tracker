#! /bin/sh

BASEDIR=$(dirname $0)
INDEX=1
DB_NAME="expense-tracker"
DB_USERNAME="expensetracker"

export PGPASSWORD=expensetracker

cd $BASEDIR

exec_first_script()
{
    echo "\n"
    echo "----- CREATING DATABASE AND USER -----"
    echo "\n"

    psql -h 127.0.0.1 -U postgres < 0.sql

    echo "\n"
    echo "----- DONE -----"
}

apply_changes()
{
    echo "\n"
    echo "----- APPLYING DATABASE CHANGES -----"
    echo "\n"

    while true
    do
        if [ -e $INDEX.sql ]
        then
            psql -h 127.0.0.1 -U $DB_USERNAME -f $INDEX.sql $DB_NAME
            INDEX=$((INDEX+1))
        else
            break
        fi
    done

    echo "\n"
    echo "----- DONE -----"
}

exec_first_script
apply_changes

cd - > /dev/null 2>&1